FROM php:7.2-apache
COPY 000-default.conf /etc/apache2/sites-available/
COPY src/ /var/www/html/

RUN a2enmod rewrite
RUN chown -Rf www-data:www-data /var/www/html

#USER www-data:www-data
WORKDIR /var/www/html

EXPOSE 80

